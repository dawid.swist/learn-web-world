This sub-project contains my exercises with vue.js
* [Lesson 01. Variable binding](lesson-01-variable-binding)
* [Lesson 02. Update variable, bind event to components](lesson-02-update-data-bind-event)
* [Lesson 03. Computed properties](lesson-03-computed-properties)

[back](../README.md)