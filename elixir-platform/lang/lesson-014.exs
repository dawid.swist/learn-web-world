# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/module-attributes.html
#
# Author: Dawid Świst
#
## Module attributes

# Module attributes in Elixir serve three purposes:

# # They serve to annotate the module, often with information to be used by the user or the VM.
# # They work as constants.
# # They work as a temporary module storage to be used during compilation.

### As annotations

defmodule Math do
  @some_attribute 2
  @moduledoc """
  Provides math-related functions.

  ## Examples

  iex> Math.sum(1, 2)
  3

  """

  @doc """
  Calculates the sum of two numbers.
  """
  def sum(a, b), do: a + b
end


### As constant

defmodule MyServer do
  @my_data 14
  def first_data, do: @my_data
  @my_data 13
  def second_data, do: @my_data
end

MyServer.first_data
# => 14
MyServer.second_data
# => 13
