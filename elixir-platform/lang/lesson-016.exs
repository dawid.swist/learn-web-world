# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/comprehensions.html
#
# Author: Dawid Świst
#
## Comprehensions


for n <- [1,2,3,4], do: n*n
# => [1, 4, 9, 16]


### Generators and filters


values = [good: 1, good: 2, bad: 3, good: 4]

for {:good, n} <- values, do: n*n
# => [1, 4, 16]

multiple_of_3? = fn(n) -> rem(n, 3) == 0 end
for n <- 0..5, multiple_of_3?.(n), do: n * n
# => [0, 9]

### The :into option

for {key, val} <- %{"a" => 1, "b" => 2}, into: %{}, do: {key, val * val}
# => %{"a" => 1, "b" => 4}
