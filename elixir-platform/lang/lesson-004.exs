# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/pattern-matching.html
#
# Author: Dawid Świst
#

# Pattern matching


{a, b, c} = {:hello, "world", 42}
IO.puts "Tuple: a=#{a}, b=#{b}, c=#{3}"

[a, b, c] = [1, 2, 3]
IO.puts "List: a=#{a}, b=#{b}, c=#{3}"

[h | t] = [1,2,3]

IO.puts "h=#{h}, t=#{t}"

