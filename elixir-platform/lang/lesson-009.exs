# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/recursion.html
#
# Author: Dawid Świst
#
## Recursion


### Loops through recursion

defmodule Recursion do
  def print_multiple_times(msg, n) when n <= 1 do
    IO.puts msg
  end

  def print_multiple_times(msg, n) do
    IO.puts msg
    print_multiple_times(msg, n - 1)
  end
end

Recursion.print_multiple_times("Hello",3)
# => Hello
# => Hello
# => Hello
# => :ok

### Reduce and map algorithms

defmodule Math2 do
  def sum_list([head | tail], accumulator) do
    sum_list(tail, head + accumulator)
  end

  def sum_list([], accumulator) do
    accumulator
  end
end

Math2.sum_list([1,2,3,4,5],0)
# => Hello
# => Hello
# => Hello
# => 15


