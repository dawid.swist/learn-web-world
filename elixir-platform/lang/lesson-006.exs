# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/binaries-strings-and-char-lists.html
#
# Author: Dawid Świst
#

### Binaries, strings, and charlists


## String

some_string = "Hello"

is_binary some_string
# => true

byte_size(some_string)
# => 5

String.length some_string
# => 5


## Binaries (and bitstrings)

# => <<0, 1, 2, 3>>

String.valid?(<<0,1,2,3,4>>)
# => true

String.valid?(<<239, 191, 19>>)
# => false

<<0, 1, x>> = <<0, 1, 2>>
x
# => 2
