# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/case-cond-and-if.html
#
# Author: Dawid Świst
#

## case

case {1,2,3} do
  {4, 5, 6} -> "This clause won't match"
  {1, x, 3} -> "This clause will match and bind x to #{x} in this clause"
  _ -> "This clause would match any value"
end
# => "This clause will match and bind x to 2 in this clause"

## Pattern match with pin

x = 1

case 10 do
  ^x -> "won't match"
  _ -> "match"
end

# => "match"


## Case with anonymous functions

f = fn
  x,y when x > 0 -> x+y
  x,y -> x * y
end

f.(1,3)

# => 4

## cond operation
x=10
cond do
  x==1  -> "x=1"
  x==2  -> "x=2"
  x==10 -> "x=10"
  true -> nil
end
# => "x=10"

## if, else, unless

if true do
  "This works!"
end
# => "This works!"

if nil do

  "is nil"
else

  "is OK, Is not Nil"
end
# => "is OK, Is not Nil"

if true, do: 1+1, else: 1+2
# => 2
