# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/keywords-and-maps.html
#
# Author: Dawid Świts
#

### Keyword lists and maps

## Keyword lists

list = [{:a,1}, {:b,2}]
# => [a: 1, b: 2]

list == [a: 1, b: 2]
# => true

list2_as_keyword = [c: 3, d: 4]
# => [c: 3, d: 4]

list ++ [c: 3]
# => [a: 1, b: 2, c: 3]

## Maps
map = %{:a => 1, 2 => :b}
# => %{2 => :b, :a => 1}

map[:a]
# => 1

map[2]
# => :b

map = %{:a => 1, 2 => :b}
# => %{2 => :b, :a => 1}

map = %{map | 2 => "two"}
# => %{2 => "two", :a => 1}

map2 = %{:a => 1, :b =>2}
map2.a
# => 1

