# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/structs.html and https://elixir-lang.org/getting-started/protocols.html
#
# Author: Dawid Świst
#
## Structs and protocols

defmodule User2 do
  @enforce_keys [:name,:age]
  defstruct [:name, :age, state: "free"]
end


defprotocol Size do
  @doc "Calculates the size (and not the length!) of a data structure"
  def size(data)
end

defimpl Size, for: User2 do
  def size(data), do: 2
end

defmodule Main do
  def main do
    u_dawid = %User2{name: "Dawid", age: 29 }
    u_dawid.name # => "Dawid"

    u_marta = %User2{u_dawid | name: "Marta"}
    u_marta.name
    # => "Marta"

    Size.size(u_marta)
    # => 2
  end
end

Main.main

# => [33mwarning: [0mvariable "data" is unused
# =>   nofile:20
# => 2
