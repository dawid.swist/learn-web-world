# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/processes.html
#
# Author: Dawid Świst
#
## Processes

### Spawn

self()
# => #PID<0.133.0>

pid = spawn fn -> 1+2 end
# => #PID<0.152.0>

Process.alive?(pid)
# => false

### send and receive

send self(), {:hello, "world"}
receive do
  {:hello, msg} -> msg
  {:world, msg} -> "won't match"
end
# => "world"

### Task and state

defmodule KV do
  def start_link do
    Task.start_link(fn -> loop(%{}) end)
  end

  defp loop(map) do
    receive do
      {:get, key, caller} ->
        send caller, Map.get(map, key)
        loop(map)
      {:put, key, value} ->
        loop(Map.put(map, key, value))
    end
  end
end

send pid, {:put, :hello, :world}
send pid, {:get, :hello, self()}
# => {:get, :hello, #PID<0.133.0>}
# iex> flush()
# :world
# :ok
