# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/basic-operators.html
#
# Author: Dawid Świst
#

IO.puts "[1, 2, 3] ++ [4, 5, 6] = #{[1, 2, 3] ++ [4, 5, 6]}"

IO.puts "fist test" <> " " <> "second text"

IO.puts "true and true = #{true and true}"

IO.puts "false or is_atom(:example) = #{false or is_atom(:example)}"

IO.puts "1 == 1.0 = %{1 == 1.0}"

IO.puts "1 == 1.0 = #{1 == 1.0}"

IO.puts "1 === 1.0 = #{1 === 1.0}"

IO.puts "1 || true = #{1 || true}"

IO.puts "true && 17 = #{true && 17}"
