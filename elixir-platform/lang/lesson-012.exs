# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/io-and-the-file-system.html
#
# Author: Dawid Świst
#
## IO system

### IO package

# some_value_from_io = IO.gets"prompt some value"
# IO.puts some_value_from_io

### File module

{:ok, file} = File.open "/tmp/lesson-012-file.txt", [:write]

IO.binwrite(file, "hello world")

File.close file

File.read "/tmp/lesson-012-file.txt"
# => {:ok, "hello world"}
File.read! "/tmp/lesson-012-file.txt"
# => "hello world"
File.read! "/tmp/path-to-some-unknown-file"
# => %File.Error{
# =>   action: "read file",
# =>   path: "/tmp/path-to-some-unknown-file",
# =>   reason: :enoent
# => }

File.read "/tmp/path-to-some-unknown-file"
# => {:error, :enoent}

### PATH

Path.basename '.'
