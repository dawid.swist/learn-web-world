# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/basic-types.html
#
# Author: Dawid Świst
#


# Basic type
var_integer1 = 1          # integer
var_ineger2  = 0x1F       # integer
var_float    = 1.0        # float
var_boolean  = true       # boolean
var_atom     = :atom      # atom / symbol
var_string   = "elixir"   # string
var_list     = [1, 2, 3]  # list
var_tuple    = {1, 2, 3}  # tuple


# Basic operations

IO.puts (1 + 1)
IO.puts (5 * 5)
IO.puts (10 / 2)
IO.puts ("div(10,2): #{div(10,2)}")

IO.puts ("0b1010: #{0b1010}")
IO.puts ("0o777: #{0o777}")
IO.puts ("0x1F: #{0x1F}")

# Elixir supports true and false as booleans:

IO.puts "true==true: #{true==true}"
IO.puts "false==true: #{false==true}"
IO.puts "is_boolean(true): #{is_boolean(true)}"
IO.puts "is_boolean(1): #{is_boolean(1)}"

# Atoms. An atom is a constant whose name is its own value.

IO.puts ":hello = #{:hello}"
IO.puts ":hello == :ok #{:hello == :ok}"
IO.puts "is_atom(:hello) = #{is_atom(:hello)}"

# String

some_string =  "some String"
IO.puts some_string
IO.puts "iold Bundle s_binary(some_string) = #{is_binary(some_string)}"
IO.puts "byte_size(some_string) = #{byte_size(some_string)}"
IO.puts "String.length(some_string) = #{String.length(some_string)}"


# Anonymous functions
add = fn a, b -> a + b end

IO.puts("add.(1, 2) = #{add.(1, 2)}")
IO.puts("is_function(add) = #{is_function(add)}")

# List

some_list = [1, 2, true, 3]
IO.puts "length(some_list) = #{length some_list}"
IO.puts ([1, 2, 3] ++ [4, 5, 6])
([1, true, 2, false, 3, true] -- [true, false])
hd([1,2,3,4])

# tuples
some_tuble = {1, 2, :OK}

IO.puts some_tuble
