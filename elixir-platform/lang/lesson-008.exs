# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/modules-and-functions.html
#
# Author: Dawid Świst
#

### Modules and functions

defmodule Math do

  def sum(a,b), do: a+b
  def inc(x, i \\ 1), do: x + i

  def multi(a,b) do
    a * b
  end

  def div(a,1), do: a
  def div(a,b) when b>0, do: a/b

  def zero?(0), do: true
  def zero?(x) when is_integer(x), do: false
end

Math.sum(1,2)
# => 3

Math.multi(2,3)
# => 6

Math.div(2,1)
# => 2
Math.div(4,3)
# => 1.3333333333333333

Math.zero?(0)
# => true

Math.zero?(-1)
# => false


Math.inc(10)
# => 11

Math.inc(10,10)
# => 20

ref_to_inc = &Math.inc/2

ref_to_inc.(1)

