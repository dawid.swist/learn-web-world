# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/alias-require-and-import.html
#
# Author: Dawid Świst
#
## alias, require, and import

# Alias the module so it can be called as Bar instead of Foo.Bar
#alias Foo.Bar, as: Bar

# Require the module in order to use its macros
#require Foo

# Import functions from Foo so they can be called without the `Foo.` prefix
#import Foo

# Invokes the custom code defined in Foo as an extension point
#use Foo


defmodule Math2 do
  defmodule Num do
    def plus(x,y) do
      x+y
    end
  end

  defmodule Bin do
    def plus_bin(x,y) do
      x+y
    end
  end
end

### alias

alias Math2.Num, as: Mnum

Math2.Num.plus(1,2)

Mnum.plus(2,3)
# => 5

### require

#Integer.is_odd(2)
# => %UndefinedFunctionError{
# =>   arity: 1,
# =>   exports: nil,
# =>   function: :is_odd,
# =>   module: Integer,
# =>   reason: nil
# => }
require Integer
Integer.is_odd(2)
# => false


### import
import List, only: [duplicate: 2]
duplicate :ok, 3
# => [:ok, :ok, :ok]
