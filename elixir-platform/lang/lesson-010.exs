# Welcome to my samples related to The elixir language.
# Source code related to https://elixir-lang.org/getting-started/enumerables-and-streams.html
#
# Author: Dawid Świst
#
## Enumerables and Streams

### Enumerables

Enum.map([1,2,3,4], fn x-> x*x end)
# => [1, 4, 9, 16]

Enum.map %{ :a=>'a', :b=>'b', :c=>'c' }, fn {k,v} -> v end
# => ['a', 'b', 'c']

Enum.reduce 1..6, fn x,y-> x+y end
# => 21

### Eager vs Lazy and The pipe operator

total_sum = 1..10000 |> Enum.map(fn x -> x *3 end) |> Enum.filter(fn x -> rem(x , 2) !=0 end) |> Enum.sum
# => 75000000

### Stream. Lazy list
1..20 |> Stream.map(fn x-> x*11 end) |> Stream.filter(&(rem(&1, 2)!=0))
# => #Stream<[
# =>   enum: 1..20,
# =>   funs: [#Function<48.129278153/1 in Stream.map/2>,
# =>    #Function<40.129278153/1 in Stream.filter/2>]
# => ]>

1..2000000000 |> Stream.map(fn x-> x*11 end) |> Stream.filter(&(rem(&1, 2)!=0))
# => #Stream<[
# =>   enum: 1..2000000000,
# =>   funs: [#Function<48.129278153/1 in Stream.map/2>,
# =>    #Function<40.129278153/1 in Stream.filter/2>]
# => ]>

stream = Stream.cycle([1, 2, 3])
Enum.take(stream, 10)
# => [1, 2, 3, 1, 2, 3, 1, 2, 3, 1]
